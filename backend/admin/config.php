<?php
$configdb = [
    'username' => 'root',
    'password' => '',
    'dbname' => 'monitor',
    'host' => 'localhost'
];

function getConnection($configdb){
    $conn = new mysqli($configdb['host'], $configdb['username'], $configdb['password'], $configdb['dbname']);
    return $conn;
}
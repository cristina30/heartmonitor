<h2>Timeline</h2>

<?php
require_once 'config.php';
?>
<div id="visualization"></div>
<input type="button" name="delete_database" value="clear" id="delete_database_btn" onclick="delete_content()"/>
<input type="button" name="update_arduino" value="update" id="update_arduino_btn" onclick="update_content()"/>



<style>
    body,
    html {
        font-family: arial, sans-serif;
        font-size: 11pt;
    }

    #visualization {
        box-sizing: border-box;
        width: 100%;
        height: 300px;
    }


    /** drag and drop */
    li.item {
        list-style: none;
        width: 150px;
        color: #1A1A1A;
        background-color: #D5DDF6;
        border: 1px solid #97B0F8;
        border-radius: 2px;
        margin-bottom: 5px;
        padding: 5px 12px;
    }

    li.item:before {
        content: "≣";
        font-family: Arial, sans-serif;
        display: inline-block;
        font-size: inherit;
        cursor: move;
    }

    li.object-item {
        list-style: none;
        width: 150px;
        color: #1A1A1A;
        background-color: #D5DDF6;
        border: 1px solid #97B0F8;
        border-radius: 2px;
        margin-bottom: 5px;
        padding: 5px 12px;
    }

    li.object-item:before {
        content: "≣";
        font-family: Arial, sans-serif;
        display: inline-block;
        font-size: inherit;
        cursor: move;
    }

    .items-panel {
        display: flex;
        justify-content: space-around;
    }
</style>

<script src="https://visjs.github.io/vis-timeline/standalone/umd/vis-timeline-graph2d.min.js"></script>
<link href="https://visjs.github.io/vis-timeline/styles/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />


<?php
$conn = getConnection($configdb);
$result = $conn->query("SELECT * FROM notifications");
$notifications_json = [];
echo '<ul id="sortable" style="list-style-type:none; max-width: 50%;">';
while ($row = $result->fetch_assoc()) {
    $notifications_json[] = $row;
}
$notifications_json = json_encode($notifications_json);

?>


<script>
    var notifications = <?= $notifications_json ?>;
    console.log('notificatons: ', notifications);
    var groups = new vis.DataSet(
        [{
                id: 0,
                content: 'Timeline',
                value: 1
            },
        ]);

        let min = 9999;
        let max = 0;
        for(let notification of notifications){
            if(notification.start < min){
                min = notification.start
            }
            if(notification.end > max){
                max = notification.end
            }
        }
        console.log('min: ', min);
        console.log('max: ', max);

//   max: new Date(2020, 2, 12),

    let ds = [];
    for (let n of notifications) {
        ds.push({
            id: n.id,
            group: 0,
            content: n.notification_type,
            start: new Date(2014, 3, n.start),
            end: new Date(2014, 3, n.end)
        });
    }

    // create a dataset with items
    // note that months are zero-based in the JavaScript Date object, so month 3 is April
    var itemsOnView = new vis.DataSet(ds);

    // create visualization
    var container = document.getElementById('visualization');
    var options = {
        // option groupOrder can be a property name or a sort function
        // the sort function must compare two groups and return a value
        //     > 0 when a > b
        //     < 0 when a < b
        //       0 when a == b
        groupOrder: function(a, b) {
            return a.value - b.value;
        },
        showMinorLabels : false,
        showMajorLabels : false,
        editable: true,
        showCurrentTime: true,
        min: new Date(2014, 3, 1), // lower limit of visible range
        max: new Date(2014, 3, 30),
    };
    
    

    var timeline = new vis.Timeline(container);


    timeline.on('select', function(event) {
        console.log('you ve double clicked');
        let searchForItem = itemsOnView.get(event.item);
        console.log('clicked on: ', event.items[0])
        $('#view-details').html('<a href="/backend/admin/index.php?path=details&id='+event.items[0]+'">View details</a>');
    })


    timeline.on('mouseUp', function(event) {
        let itemIndex = parseInt(event.item) - 1;
        let item = itemsOnView[itemIndex];
        // console.log('search for: ', itemsOnView.get(event.item));
        let searchForItem = itemsOnView.get(event.item);

        if (Array.isArray(searchForItem)) {
            alert('Could not change event date, please try again');
            return;
            // console.log('MULTIPLE');

            // for (let el of searchForItem) {
            //     console.log('ELEMENT: ', el);
            //     console.log('id: ', event.item);
            //     if (el.id == ("" + event.item)) {
            //         searchForItem = el;
            //         console.log('FOUND EL: ', el);
            //     }
            // }
        } else {
            console.log('SEND TO SERVER: ', searchForItem);
            console.log('SINGLE');
            let urlVal = '/backend/admin/save-notification-modification.php?id=' + searchForItem.id + '&start=' + searchForItem.start.getDate() + '&end=' + searchForItem.end.getDate();
            console.log('url value: ', urlVal);
            $.ajax({
                url: urlVal,
                success: function(date) {
                    console.log('date: ', date)

                    // TEST set id
                    searchForItem.id = date['last_id'];
                    console.log('modifications search for item: ', searchForItem);
                }
            });
        }

    });
    timeline.setOptions(options);
    timeline.setGroups(groups);
    timeline.setItems(itemsOnView);
    timeline.addCustomTime(new Date());
</script>

<!-- <div style="margin-top:400px;">
</div> -->
<div id="view-details">
</div>
<div class='side'>
    <h3>Items:</h3>
    <ul class="items">

        <li draggable="true" class="item">
            heat - range
        </li>
        <li draggable="true" class="item">
            vibration - range
        </li>
        <!-- <li draggable="true" class="item">
            item 3 - range - fixed times -
            <br> (start: now, end: now + 10 min)
        </li> -->
    </ul>
</div>

<div id="output"></div>


<script>
    /* drag and drop */

    function handleDragStart(event) {
        var dragSrcEl = event.target;

        event.dataTransfer.effectAllowed = 'move';
        var itemType = event.target.innerHTML.split('-')[1].trim();
        var item = {
            id: new Date(),
            type: itemType,
            content: event.target.innerHTML.split('-')[0].trim()
        };
        // set event.target ID with item ID
        event.target.id = new Date(item.id).toISOString();

        var isFixedTimes = (event.target.innerHTML.split('-')[2] && event.target.innerHTML.split('-')[2].trim() == 'fixed times')
        if (isFixedTimes) {
            item.start = new Date();
            item.end = new Date(1000 * 60 * 10 + (new Date()).valueOf());
        }
        event.dataTransfer.setData("text", JSON.stringify(item));

        // Trigger on from the new item dragged when this item drag is finish
        event.target.addEventListener('dragend', handleDragEnd.bind(this), false);
    }

    function handleDragEnd(event) {
        console.log('event: ', event);
        console.log('event id: ', event.target.id)
        // Last item that just been dragged, its ID is the same of event.target
        var newItem_dropped = timeline.itemsData.get(event.target.id);

        var html = "<b>id: </b>" + newItem_dropped.id + "<br>";
        html += "<b>content: </b>" + newItem_dropped.content + "<br>";
        html += "<b>start: </b>" + newItem_dropped.start + "<br>";
        html += "<b>end: </b>" + newItem_dropped.end + "<br>";
        document.getElementById('output').innerHTML = html;


        let notificationType = newItem_dropped.content;
        let tp = notificationType == 'vibration' ? 'pulses' : null;

        let urlVal = `/backend/admin/insert-notification-type.php?notification_type=${notificationType}&type=${tp}&start=${newItem_dropped.start.getDate()}&end=${newItem_dropped.end.getDate()}`;
        console.log('URL: ', urlVal);
        $.ajax({
            url: urlVal,
            success: function(data) {
                console.log('server data: ', data);
                newItem_dropped.id = data.last_id;
            }
        });

    }

    function handleObjectItemDragStart(event) {
        var dragSrcEl = event.target;

        event.dataTransfer.effectAllowed = 'move';
        var objectItem = {
            content: 'objectItemData',
            target: 'item'
        };
        event.dataTransfer.setData("text", JSON.stringify(objectItem));
    }


    function delete_content() {
        console.log("deleting content!");

        $.ajax({
            type: "POST",
            url: "delete-database.php",
            success: function (obj) {
                console.log(obj);
            }
        });
	}

    function update_content() {
        console.log("updating content!");

        $.ajax({
            type: "POST",
            url: "arduino-endpoint-read-data.php",
            success: function (obj) {
                console.log(obj);
            }
        });
    }


    var items = document.querySelectorAll('.items .item');

    var objectItems = document.querySelectorAll('.object-item');

    for (var i = items.length - 1; i >= 0; i--) {
        var item = items[i];
        item.addEventListener('dragstart', handleDragStart.bind(this), false);
    }

    for (var i = objectItems.length - 1; i >= 0; i--) {
        var objectItem = objectItems[i];
        objectItem.addEventListener('dragstart', handleObjectItemDragStart.bind(this), false);
    }
</script>
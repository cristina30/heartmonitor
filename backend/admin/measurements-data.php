<?php

header('Content-Type: application/json');

    require_once 'config.php';

    // $id = $_REQUEST['id'];
    // $duration = $_REQUEST['duration'];
    // $minValue = $_REQUEST['min_value'];
    // $maxValue = $_REQUEST['max_value'];

    $conn = getConnection($configdb);
    // $conn->query("UPDATE notifications set duration = $duration, min_value = $minValue, max_value = $maxValue WHERE id = $id");
    // header('Location: index.php?path=notifications');


    $labelsAndData = [
        'labels' => [],
        'data' => []
    ];
    $results = $conn->query("SELECT * FROM measurements");
    while ($row = $results->fetch_assoc()) {
        $labelsAndData['labels'][] = $row['measurement_time'];
        $labelsAndData['data'][] = $row['value'];
    }

    echo json_encode($labelsAndData);

    


    
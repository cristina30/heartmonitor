<h2>Notification details</h2>

<?php
require_once 'config.php';
$conn = getConnection($configdb);
$id = $_REQUEST['id'];
$result = $conn->query("SELECT * FROM notifications WHERE id = $id");

$not = null;
while ($row = $result->fetch_assoc()) {
    $not = $row;
}
// print_r($not);


?>


ID: <?= $not['id'] ?>
<br>
Notification type: <?= $not['notification_type'] ?>
<br>
Start: <?= $not['start'] ?>
<br>
End:
<?= $not['end'] ?>
<br>






<style>
    canvas {
        background-color: #eee;
        /* position: absolute; */
        margin: auto;

    }
</style>
<!-- PATTERN CANVAS -->
<div style="max-width: 70%; ">
    <canvas id="chartJSContainer" style="max-height: 500px; width: 300px;"></canvas>
</div>



<script>
    function generateChart(dataValues) {



        var options = {
            type: 'line',
            data: {
                labels: ["T0", "T1", "T2", "T3", "T4"],
                datasets: [{
                    label: 'Intensity',
                    data: dataValues,
                    borderWidth: 1,
                    pointHitRadius: 25
                }]
            },
            zoomMin: 1000 * 60 * 60 * 24 * 5,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 25,
                            min: 0
                        }
                    }]
                },
                responsive: true,
                dragData: true,
                onDragStart: function(e) {
                    //console.log(e.clientY)
                    //console.log(test.scales["y-axis-0"].getValueForPixel(e.clientY))
                },
                onDrag: function(e, datasetIndex, index, value) {
                    //console.log(e.clientY)
                },
                onDragEnd: function(e, datasetIndex, index, value) {
                    console.log(datasetIndex, index, value);
                    console.log('selected notification: ', selectedNotification);
                    var patternValues = selectedNotification.pattern_values.split(',');
                    patternValues[index] = value;
                    console.log('new patterns: ', patternValues);
                    var infoUpdatePatterns = patternValues.join();
                    // console.log('modifying element at: ');
                    var url = 'edit-value-pattern-notification.php?newpattern=' + infoUpdatePatterns + '&index=' + index + '&selected_pattern_id=' + selectedPatternId;
                    console.log('getting dta from: ', url);
                    $.ajax({
                        url: url,
                        success: function(serverData) {
                            console.log('server data: ', serverData);
                            selectedNotification.pattern_values = serverData.pattern_values;
                        }
                    });
                }
            }
        }

        var ctx = document.getElementById('chartJSContainer').getContext('2d');
        window.test = new Chart(ctx, options);
    }
</script>

<script>
    var not = <?= json_encode($not) ?>;

    var selectedNotification = not;
    var selectedPatternId = not.id;
    var patternValues = not.pattern_values.split(',');
    generateChart(patternValues);
</script>

<div id="slider">
    <div id="custom-handle" class="ui-slider-handle"></div>
</div>


<script>
    if (not.notification_type == 'heat') {
        $('#chartJSContainer').hide();
        $(function() {
            var handle = $("#custom-handle");
            $("#slider").slider({
                change: function(e, ui) {
                    console.log('changed to: ', ui.value);
                    $.ajax({
                        url: '/backend/admin/save-notification-heat-modification.php?id=' + not.id + '&max_value=' + ui.value,
                        success: function(data) {
                            console.log('server: ', data);
                        }
                    });
                },
                'value': not.max_value,
                create: function() {
                    handle.text($(this).slider("value"));
                },
                slide: function(event, ui) {
                    handle.text(ui.value);
                }
            });
        });
    }
</script>
<hr>
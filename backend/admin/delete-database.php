<?php
require_once 'config.php';
?>
<?php
    $conn = getConnection($configdb);

    if ($conn->errno) {
        echo "unable to connect";
        exit();
    }
    else {
        echo "connected!";
    }
    
    $sql = "DELETE FROM notifications";
    
    if ($conn->query($sql) === TRUE) {
        echo "deleted the database!";
    }
    else {
        echo "uh oh!";
    }

    $conn->close();
?>
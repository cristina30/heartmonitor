<?php
    header('Content-Type: application/json');
    require_once 'config.php';
    $connNotifications = getConnection($configdb);
    $connMeasurements = getConnection($configdb);


    $notifications = $connNotifications->query("SELECT * FROM notifications WHERE is_active = 1 ORDER BY priority ASC");
    $measurements = $connMeasurements->query("SELECT * FROM measurements order by id desc limit 5");

    $result = [
        'notifications' => [],
        'measurements' => [],
        'test' => 'hello'
    ];
    while ($row = $notifications->fetch_assoc()) {
        // id notification_type min_value max_value duration
        $array_values = $row['pattern_values'];
        $array_values = explode(",", $array_values);
        $array_values_as_number = [];
        foreach($array_values as $value){
            $array_values_as_number[] = floatval($value);
        }
        $result['notifications'][] = ['id' => intval($row['id']), 'notification_type' => $row['notification_type'],
                'min_value' => $row['min_value'], 'max_value' => $row['max_value'], 'duration' => $row['duration'], 'type' => $row['type'],
            'is_active' => $row['is_active'], 'pattern_values' => $array_values_as_number,
        'start' => intval($row['start']), 'end' => intval($row['end'])];
                
    }

    while($row = $measurements->fetch_assoc()){
        $result['measurements'][] = $row;
    }

    
    echo json_encode($result);

?>
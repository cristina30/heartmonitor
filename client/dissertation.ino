// latest

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <Hash.h>
#include <FS.h>


#include <Servo.h>
#include <LOLIN_I2C_MOTOR.h>
#include <ArduinoJson.h>


#define DEBUG 1
#define NORMAL_HEARTBEAT 70

// network wifi configuration
const char *ssid = "Christine";
const char *password = "cyzx1333";

// A few variables which we'll need throughout the program
int16_t thisRead = 0; // The current pot value
int16_t lastRead = 0; // The last pot value (this is used to prevent sending duplicate values)
uint8_t counter = 0;  // Used to limit how often we send pot updates via websockets

// Initialise websockets, web server and servo
// WebSocketsServer webSocket = WebSocketsServer(81);
// ESP8266WebServer server(80);
Servo myservo;
LOLIN_I2C_MOTOR motor;

IPAddress server_php(192,168,43,87); 
WiFiClient client;

// Timer values
unsigned long startTime;

/*
 * This structure represents notifications as read from the server endpoint backend/admin/arduino-endpoint-read-data.php
*/
struct Notification{

  int id;
  String notification_type;
  int min_value;
  int max_value;
  int duration;
  int start;
  int end;
  String type;
  int is_active;
  double pattern_values[5];
  
};


/*
 * This structure represents measurements as read from the server endpoint backend/admin/arduino-endpoint-read-data.php
 **/
struct Measurement{
  
  int id;
  int value;
  String measurement_time;
  
};

typedef struct Notification Notification;
typedef struct Measurement Measurement;

// structure which stores notifications and measurements
struct ServerResponse{
    Notification* notifications;
    Measurement* measurements;
  };


int number_of_notifications = 0;
int number_of_measurements = 0;



typedef struct ServerResponse ServerResponse;

/**
 * This function makes a request to the server endpoint, "downloads" the data into a 
 * ServerResponse instance
 * **/
ServerResponse* read_notifications_and_measurements_from_server(){
  // send a test request to the server ----------------------------
  
   
    
    if (client.connect(server_php, 80)) { // connect to the server
      
      client.println("GET /backend/admin/arduino-endpoint-read-data.php HTTP/1.0"); // 1.0
      client.println();
    
  }
  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    
    return NULL;
  }

  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return NULL;
  }

  
  // if the request was successful, start reading the data into Notification and Measurement objects
  const size_t capacity = JSON_OBJECT_SIZE(100) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonBuffer jsonBuffer(capacity);

  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(client);
  if (!root.success()) {
    Serial.println(F("Parsing failed!"));
    return NULL;
  }

  number_of_notifications = root["notifications"].size();
  Serial.println("NUMBER OF NOTIFICATIONS: ");
  Serial.println(number_of_notifications);
  Serial.println("NUMBER OF MEASUREMENTS: ");
  number_of_measurements = root["measurements"].size();
  Serial.println(number_of_measurements);


  Notification* notifications = new Notification[number_of_notifications];  // there are 3 notifications 
  Measurement* measurements = new Measurement[number_of_measurements]; // the 'lastest' five measurements stored on the server

  // in order to save a new measurement on the server, 
  // the arudino component needs to send a request to <SERVER>/backend/admin/arduino-endpoint-save-measurement.php?measurement_value=<MEASUREMENT_VALUE> 
  // MEASUREMENT_VALUE - int, representing the heartbeat
  
  for(int i=0; i<root["notifications"].size(); i++){
    // transform json from the server into Notification instances
    Notification not1; 
    
    not1.id = root["notifications"][i]["id"].as<int>();
    not1.notification_type = String(root["notifications"][i]["notification_type"].as<char*>());
    not1.min_value = root["notifications"][i]["min_value"].as<int>();
    not1.max_value = root["notifications"][i]["max_value"].as<int>();
    
    not1.start = root["notifications"][i]["start"].as<int>();
    not1.end = root["notifications"][i]["end"].as<int>();
    
    not1.duration = root["notifications"][i]["duration"].as<int>();
    not1.type = root["notifications"][i]["type"].as<String>();
    not1.is_active = root["notifications"][i]["is_active"].as<int>();
    
    String type_heat = "heat";
       
    if(not1.type.equals(type_heat)){
      // cannot parse values for heat
    }else{
      // only for vibration types
      double pattern_values[5];
      for(int j=0; j<5; j++){
        not1.pattern_values[j] = root["notifications"][i]["pattern_values"][j];      
      }
  }
    notifications[i] = not1;
  }
  
   Serial.println(notifications[0].notification_type);
    Serial.println(notifications[0].id);
    Serial.println(notifications[0].max_value);
  
  for(int i=0; i<root["measurements"].size(); i++){
    // measurements json instances from the server are added to the measurements collection (array) on the Arduino component
    Measurement m;    
    m.id = root["measurements"][i]["id"].as<int>();    
    m.measurement_time = root["measurements"][i]["measurement_time"].as<String>();      
    m.value = root["measurements"][i]["value"].as<int>();    
    measurements[i] = m;   
  }
  
  
  ServerResponse* response = new ServerResponse;
  
  response->measurements = measurements;
  response->notifications = notifications;
  
  return response;
}

void setup(void) {


  // Arduino endpoint on the Php Apache server: http://192.168.99.1/backend/arduino-endpoint.php  
  

  Serial.begin(9600);
  Serial.print("LOADING...");
  delay(1000);
  Serial.print("STEP 0.0");
  WiFi.begin(ssid, password);
  Serial.print("STEP 0.1");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("CONNECTED");
  Serial.print("STEP 0");
  IPAddress myIP = WiFi.localIP();
  Serial.print("STEP 1");
  myservo.attach(D2);
  Serial.print("STEP 2");
  SPIFFS.begin();
  Serial.print("STEP 3");

//  while (motor.PRODUCT_ID != PRODUCT_ID_I2C_MOTOR) //wait motor shield ready.
//  {
//    motor.getInfo();
//  }
  Serial.print("STEP 4");

    
  startTime = millis();
  Serial.print("STEP 5");
}

/**
 * This function will handle the server response (containing the measurements and notifications)
 * this function will tell 
 */
void handle_server_response(ServerResponse* resp){
  Notification* notifications = resp->notifications;
  Measurement* measurements = resp->measurements;
  Serial.println(notifications[0].notification_type);
  Serial.println("--test 2--");
}

void check_heart_rate(ServerResponse* resp) {

  Serial.println("NUMBER OF NOTIFICATIONS: CHR ");
  Serial.println(number_of_notifications);
  Serial.println("NUMBER OF MEASUREMENTS: CHR ");
  Serial.println(number_of_measurements);

  Notification* notifications = resp->notifications;
  Measurement* measurements = resp->measurements;
  // check if heart rate is above 'nominal' threshold
  bool is_heart_okay = true;
  
  for(int i=0; i<number_of_measurements; i++){
    String result = "";
    result += "value: ";
    result += measurements[i].value;
    result += " time measured: ";
    result += measurements[i].measurement_time;
    Serial.println(result);
    if(measurements[i].value > NORMAL_HEARTBEAT){
      is_heart_okay = false;
      break;
    }
  }

  if(is_heart_okay){
    Serial.println("HEART OKAY");
  }else{
    Serial.println("HEART FALSE");
  }
  
  if(!is_heart_okay){
    Serial.println("SHOULD START NOTIFICATION MOTOR");
    for(int i=0; i<number_of_notifications; i++){
      if(notifications[i].notification_type == "vibration"){
        // pulses / ramps etc.
        // if(notifications[i].type == "pulses"){
           Serial.println("SHOULD NOTIFY USER VIA VIBRATION, pattern");
           // vibrate_pulses(notifications[i].duration,1,1, motor);
        // }
      }else if(notifications[i].notification_type == "heat"){
        head_notification();
      }
    }
  }
}

void stop_motor() {
  motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_STOP);
}

void start_motor() {
  motor.changeFreq(MOTOR_CH_A, 1000); //Change A & B 's Frequency to 300Hz.
  motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);
  motor.changeDuty(MOTOR_CH_A, 100);
}


bool has_notified = false;
void vibrate_pulses(int duration, int min_value, int max_value, LOLIN_I2C_MOTOR& motor){
  if(has_notified){
    return ;
  }
  int loop_counter = 0;
  bool motor_started = false;
  int number_of_iterations = 0;
  
  
  while(true){
  
    if(!motor_started && !has_notified){
      Serial.println("STARTING MOTOR");
      motor.changeFreq(MOTOR_CH_A, 300); //Change A & B 's Frequency to 300Hz.
      motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);
      motor.changeDuty(MOTOR_CH_A, 100);
      motor_started = true;
      number_of_iterations = 0;
      //has_notified = true;
    }
  
    
    loop_counter++;
    number_of_iterations++;
    if(loop_counter % 2 == 0 && motor_started){
        Serial.println("LOW FREQUENCY");
        motor.changeDuty(MOTOR_CH_A, 30);
        motor.changeFreq(MOTOR_CH_A, 100);
    }else if(loop_counter % 2 == 1 && motor_started){
      Serial.println("HIGH FREQUENCY");
      motor.changeDuty(MOTOR_CH_A, 100);
        motor.changeFreq(MOTOR_CH_A, 300);
    }
    delay(100);
    Serial.println("CI: " );
    Serial.println(number_of_iterations);
    if(number_of_iterations == duration * 10){ // vibration has lasted for a second
      number_of_iterations = 0;
      stop_motor();
      Serial.println("STOP MOTOR");
      motor_started = false;
      // has_notified = true;
      break;
    }
  }
}


void head_notification(){
  // generate heat
}



// ------------- main loop ------------------
void loop(void) {
  // Process any incoming HTTP or WebSockets requests
  // webSocket.loop();
  // server.handleClient();
  Serial.println("BEFORE REQUEST SERVER");
  ServerResponse* resp = read_notifications_and_measurements_from_server();
  check_heart_rate(resp);
  Serial.println("AFTER REQUEST SERVER");

  // check the data on the server every 15 seconds
//  if (millis() - startTime > 15000) {
//    ServerResponse* resp = read_notifications_and_measurements_from_server();
//    handle_server_response(resp);
//    startTime = millis();
//  }
    delay(5000);

 
  // test pulse
  // end test pulse


}
